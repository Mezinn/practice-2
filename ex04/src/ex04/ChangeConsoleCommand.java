/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;
import ex03.Item;
import ex03.View;
public class ChangeConsoleCommand extends ChangeItemCommand implements ConsoleCommand {
    public View view;
    public View getView() {
        return this.view;
    }
    public View setView(View view) {
        return this.view = view;
    }
    public ChangeConsoleCommand(View view) {
        this.view = view;
    }
    public char getKey() {
        return 'c';
    }
    public String toString() {
        return "'c'hange";
    }
    public void exec() {
        for (Item item : ((View) view).get_items()) {
            double m = Math.random() * 360, h = Math.random() * 360;
            System.out.println("Change item: [M - " + m + " H -" + h + "].");
            super.setItem(item);
            super.exec();
        }
        view.view_show();
    }
}
