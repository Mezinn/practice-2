/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;
import ex03.View;
public class SaveConsoleCommand implements ConsoleCommand {
    private View view;
    public SaveConsoleCommand(View view) {
        this.view = view;
    }
    public char getKey() {
        return 's';
    }
    public String toString() {
        return "'s'ave";
    }
    public void exec() {
        System.out.println("Save current.");
        try {
            this.view.save();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e);
        }
        this.view.view_show();
    }
}
