/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex03.Item;
import ex03.View;
import ex04.Command;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mezinn
 */
public class MinMaxCommand implements Command {

    private int result_min = -1;
    private int result_max = -1;
    private int progress = 0;

    private View view;

    public View getView() {
        return this.view;
    }

    public View setView(View view) {
        return this.view = view;
    }

    public MinMaxCommand(View view) {
        this.view = view;
    }

    public int getResult_min() {
        return this.result_min;
    }

    public int getResult_max() {
        return this.result_max;
    }

    public boolean running() {
        return this.progress < 100;
    }

    public void exec() {
        progress = 0;
        System.out.println("MinMax executed...");
        int idx = 0, size = view.get_items().size();
        for (Item item : view.get_items()) {
            if (item.getValue() < 0) {
                if ((this.result_max == -1)
                        || (this.view.get_items().get(result_max).getValue()
                        < item.getValue())) {
                    this.result_max = idx;
                }
            } else {
                if ((this.result_min == -1)
                        || (this.view.get_items().get(result_min).getValue()
                        > item.getValue())) {
                    this.result_min = idx;
                }
            }
            idx++;
            progress = idx * 100 / size;
            if (idx % (size / 5) == 0) {
                System.out.println("MinMax " + progress + "%");
            }
            try {
                TimeUnit.MILLISECONDS.sleep(5000 / size);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
        System.out.print("MinMax done. ");
        if (result_min > -1) {
            System.out.print("Min positive #" + result_min + " found: "
                    + String.format("%.2f.",
                            (double) this.view.get_items().get(this.result_min).getValue()));
        } else {
            System.out.print("Min positive not found.");
        }
        if (this.result_max > -1) {
            System.out.println(" Max negative #" + result_max + " found: "
                    + String.format("%.2f.",
                            (double) this.view.get_items().get(this.result_max).getValue()));
        } else {
            System.out.println(" Max negative item not found.");
        }
        progress = 100;
    }

}
