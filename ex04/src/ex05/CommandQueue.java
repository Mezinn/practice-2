package ex05;

import ex04.Command;
import ex05.Queue;
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mezinn
 */
public class CommandQueue implements Queue {

    private Vector<Command> tasks;

    private boolean waiting;

    private boolean shutdown;

    public void shutdown() {
        this.shutdown = true;
    }

    public CommandQueue() {
        this.tasks = new Vector<>();
        this.waiting = false;
        new Thread(new Worker()).start();
    }

    public void put(Command r) {
        tasks.add(r);
        if (waiting) {
            synchronized (this) {
                notifyAll();
            }
        }
    }

    public Command take() {
        if (tasks.isEmpty()) {
            synchronized (this) {
                waiting = true;
                try {
                    wait();
                } catch (InterruptedException ie) {
                    waiting = false;
                }
            }
        }
        return (Command) tasks.remove(0);
    }

    private class Worker implements Runnable {

        /**
         * Извлекает из очереди готовые к выполнению задачи; шаблон Worker
         * Thread
         */
        public void run() {
            while (!shutdown) {
                Command r = take();
                r.exec();

            }
        }
    }

}
