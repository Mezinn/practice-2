/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex03.View;
import ex04.Command;

/**
 *
 * @author mezinn
 */
public class MaxCommand implements Command {

    private int result = -1;
    private int progress = 1;

    private View view;

    public View getView() {
        return this.view;
    }

    public View setView(View view) {
        return this.view = view;
    }

    public MaxCommand(View view) {
        this.view = view;
    }

    public int getResult() {
        return this.result;
    }

    public boolean running() {
        return this.progress < 100;
    }

    public void exec() {
        this.progress = 0;
        System.out.println("Max executed...");
        int size = this.view.get_items().size();
        result = 0;
        for (int i = 0; i < size; i++) {
            if (view.get_items().get(result).getValue()
                    < view.get_items().get(result).getValue()) {
                result = i;
            }

            progress = i * 100 / size;

            if (i % (size / 3) == 0) {
                System.out.println("Max " + progress + "%");
            }

        }

        System.out.println(
                "Max done. Item #" + result
                + " found: " + view.get_items().get(result));
        progress = 100;
    }

}
