/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex04.Command;

/**
 *
 * @author mezinn
 */
public interface Queue {

    void put(Command cmd);

    Command take();

}
