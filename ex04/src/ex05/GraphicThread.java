/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex03.View;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

/**
 *
 * @author mezinn
 */
public class GraphicThread implements Runnable {

    private View view;

    public GraphicThread(View view) {
        this.view = view;
    }

    public void run() {
        System.out.println("View current.");
        int size = view.get_items().size();
        double[] xData = new double[size];
        double[] yData = new double[size];
        for (int i = 0; i < size; i++) {
            yData[i] = view.get_items().get(i).getValue();
            xData[i] = i;
        }
        XYChart chart = QuickChart.getChart("Відношення H до M", "X", "Y", "y(x)", xData, yData);

        new SwingWrapper(chart).displayChart();

        try {
            // Save it
            BitmapEncoder.saveBitmap(chart, "./Sample_Chart", BitmapEncoder.BitmapFormat.PNG);
        } catch (IOException ex) {
            Logger.getLogger(GraphicConsoleCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            // or save it in high-res
            BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (IOException ex) {
            Logger.getLogger(GraphicConsoleCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
