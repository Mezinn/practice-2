package ex05;
import ex03.Item;
import ex03.View;
import ex04.Command;
import java.util.concurrent.TimeUnit;

public class AvgCommand implements Command {
    private double result = 0.0;
    private int progress = 0;
    private View view;
    public AvgCommand(View view) {
        this.view = view;
    }
    public View getView() {
        return this.view;
    }
    public double getResult() {
        return this.result;
    }
    public boolean running() {
        return this.progress < 100;
    }
    public void exec() {
        progress = 0;
        System.out.println("Average executed...");
        result = 0.0;
        int idx = 1, size = view.get_items().size();
        for (Item item : view.get_items()) {
            result += item.getValue();
            progress = idx * 100 / size;
            if (idx++ % (size / 2) == 0) {
                System.out.println("Average " + progress + "%");
            }
            try {
                TimeUnit.MILLISECONDS.sleep(2000 / size);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
        result /= size;
        System.out.println("Average done. Result = " + String.format("%.2f", result));
        progress = 100;
    }
    public void run() {
        this.exec();
    }
}
