/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;

import org.junit.Test;
import static org.junit.Assert.*;

public class ApplicationTest {

    public ApplicationTest() {
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Application expResult = Application.getInstance();
        Application result = Application.getInstance();
        assertEquals(expResult, result);
    }
}
