/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02;

import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author mezinn
 */
public class Item implements Serializable {

    /*
    * M - Змінна об'єкта, що зберігає значення маси.
     */
    private double M;
    /*
    * H -   Змінна об'єкта, що зберігає значення висоти.
     */
    private double H;
    /*
    * Гравітаційна константа, сила притяжіння.
     */
    public static final double G = 9.8;

    /*
    * Конструктор для встановлення маси та висоти
     */
    public Item(double M, double H) {
        this.M = M;
        this.H = H;
    }

    /*
    * Повертає змінну M
     */
    public double getM() {
        return this.M;
    }

    /*
    * Повертає змінну H
     */
    public double getH() {
        return this.H;
    }

    /*
    * Повертає змінну value
     */
    public double getValue() {
        return this.M * this.H * G;
    }

    /*
    * Повертає змінну value_binary
     */
    public String getValue_binary() {
        return Integer.toBinaryString((int) this.getValue());
    }

    /*
    * Повертає змінну max_count
     */

    /*
    * Функція повертає значення змінних об'єкту для виводу на екран.
     */
    public String toString() {
        return "[\nH - " + this.H
                + "\nM - " + this.M
                + "\nvalue_binary - " + this.getValue_binary()
                + "\n]";
    }

}
