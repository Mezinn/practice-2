/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author mezinn
 */
public class ItemSerializer {

    private static final String serialized_file_name = "serialize_data.bin";

    private ArrayList<Item> items = new ArrayList<>();

    public void addItem(Item item) {
        this.items.add(item);
    }

    public Item getItem(int index) throws Exception {
        if (index < this.items.size()) {
            return this.items.get(index);
        }
        throw new Exception("Undefined index of array.");
    }

    /*
    * Функція для серіалізації масиву об'єктів
     */
    public void serialize() throws FileNotFoundException, IOException {
        try (ObjectOutputStream object_output_stream = new ObjectOutputStream(new FileOutputStream(serialized_file_name))) {
            object_output_stream.writeObject(this.items);
            object_output_stream.flush();
            object_output_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    /*
    * Функція для дескріалізації масиву об'єктів
     */
    public void unserialize() throws FileNotFoundException, IOException, ClassNotFoundException {
        try (ObjectInputStream object_input_stream = new ObjectInputStream(new FileInputStream(serialized_file_name))) {
            this.items = (ArrayList<Item>) object_input_stream.readObject();
            object_input_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

}
