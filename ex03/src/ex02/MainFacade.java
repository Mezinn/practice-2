/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author mezinn
 */
public class MainFacade {

    protected ItemSerializer item_serializer = new ItemSerializer();

    /*
    * Функція для виводу меню (обгортка). 
     */
    public void run() throws Exception {
        this.menu();
    }

    /*
    *  Функція для виводу меню на екран
     */
    protected void menu() throws Exception {
        String input_string = null;
        BufferedReader buffer_reader = new BufferedReader(new InputStreamReader(System.in));
        do {
            do {
                System.out.println("Enter command...");
                System.out.print("'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore: ");
                try {
                    input_string = buffer_reader.readLine();
                } catch (IOException e) {
                    System.out.println("Error: " + e);
                    System.exit(0);
                }
            } while (input_string.length() != 1);
            switch (input_string.charAt(0)) {
                case 'q':
                    System.out.println("Exit.");
                    break;
                case 'v':
                    System.out.println("View current.");
                    this.print_items(this.item_serializer.getItems());
                    break;
                case 'g':
                    System.out.println("Random generation.");
                    for (int i = 0; i < Math.round(Math.random() * 360.0); i++) {
                        this.item_serializer.addItem(new Item(Math.random() * 360.0, Math.random() * 360.0));
                    }
                    this.print_items(this.item_serializer.getItems());
                    break;
                case 's':
                    System.out.println("Save current.");
                    try {
                        this.item_serializer.serialize();
                    } catch (IOException e) {
                        System.out.println("Serialization error: " + e);
                    }
                    this.print_items(this.item_serializer.getItems());
                    break;
                case 'r':
                    System.out.println("Restore last saved.");
                    try {
                        this.item_serializer.unserialize();
                    } catch (Exception e) {
                        System.out.println("Serialization error: " + e);
                    }
                    this.print_items(this.item_serializer.getItems());
                    break;
                default:
                    System.out.print("Wrong command. ");
            }
        } while (input_string.charAt(0) != 'q');
    }

    public void print_items(ArrayList<Item> items) {
        System.out.println("Items:");
        items.forEach((item) -> {
            System.out.println(item);
        });
    }
}
