/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02;

import java.util.ArrayList;
import java.util.Formatter;

/**
 *
 * @author mezinn
 */
public class FormattedMainFacade extends MainFacade {

    public static final int DEFAULT_WIDTH = 20;

    private int width = DEFAULT_WIDTH;

    /*
    *   Метод для рендерінгу елементів
     */
    public void print_items(ArrayList<Item> items) {
        this.outLineLn();
        this.outHeader();
        this.outLineLn();
        this.outBody();
        this.outLineLn();
    }

    /*
    *   Метод для рендерінгу "тіла" таблиці
     */
    public void outBody() {
        Formatter formatter = new Formatter();
        formatter.format("%s%d%s%2$d%s", "%", (this.width - 3) / 2, ".0f | %", ".3f\n");
        for (Item item : this.item_serializer.getItems()) {
            System.out.printf(formatter.toString(), item.getM(), item.getH());
        }
    }

    /*
    *   Метод для рендерінгу лінії
     */
    private void outLine() {
        for (int i = this.width; i > 0; i--) {
            System.out.print('-');
        }
    }

    /*
    *   Метод для рендерінгу заголовку таблиці
     */
    private void outHeader() {
        Formatter formatter = new Formatter();
        formatter.format("%s%d%s%2$d%s", "%", (this.width - 3) / 2, "s | %", "s\n");
        System.out.printf(formatter.toString(), "m ", "h ");
    }

    /*
    *   Метод для рендерінгу лінії, з переносом на новий рядок
     */
    private void outLineLn() {
        this.outLine();
        System.out.println();
    }

    /*
    * Метод для встановлення ширини
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /*
    *   Метод для отримання ширини
     */
    public int getWidth() {
        return width;
    }

}
