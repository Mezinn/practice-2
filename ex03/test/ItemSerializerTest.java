/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ex02.Item;
import ex02.ItemSerializer;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mezinn
 */
public class ItemSerializerTest {

    public ItemSerializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addItem method, of class ItemSerializer.
     */
    @Test
    public void testAddItem() {
        System.out.println("addItem");
        Item item = new Item(0, 0);
        ItemSerializer instance = new ItemSerializer();
        int exp_result = 1;
        instance.addItem(item);
        int result = instance.getItems().size();
        assertEquals(exp_result, result);
    }

    /**
     * Test of getItem method, of class ItemSerializer.
     */
    @Test
    public void testGetItem() throws Exception {
        System.out.println("getItem");
        int index = 0;
        ItemSerializer instance = new ItemSerializer();
        Item item = new Item(0, 0);
        instance.addItem(item);
        assertEquals(item, instance.getItem(index));
    }

    /**
     * Test of serialize method, of class ItemSerializer.
     */
    @Test
    public void testSerialize() throws Exception {
        System.out.println("serialize");
        ItemSerializer instance = new ItemSerializer();
        instance.addItem(new Item(0, 0));
        instance.serialize();
    }

    /**
     * Test of unserialize method, of class ItemSerializer.
     */
    @Test
    public void testUnserialize() throws Exception {
        System.out.println("unserialize");
        ItemSerializer instance = new ItemSerializer();
        Item item = new Item(0, 0);
        instance.addItem(item);
        instance.serialize();
        instance.unserialize();
    }

    /**
     * Test of getItems method, of class ItemSerializer.
     */
    @Test
    public void testGetItems() {
        System.out.println("getItems");
        ItemSerializer instance = new ItemSerializer();
        ArrayList<Item> test_array_list = new ArrayList<>();
        test_array_list.add(new Item(0, 0));
        test_array_list.add(new Item(10, 10));
        ArrayList<Item> expResult = (ArrayList<Item>) test_array_list;
        ArrayList<Item> result = instance.getItems();
    }

}
