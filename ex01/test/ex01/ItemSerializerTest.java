/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mezinn
 */
public class ItemSerializerTest {

    public ItemSerializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of serialize method, of class ItemSerializer.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSerialize() throws Exception {
        System.out.println("serialize");
        ItemSerializer instance = new ItemSerializer(new Item(0, 0));
        instance.serialize();
    }

    /**
     * Test of unserialize method, of class ItemSerializer.
     */
    @Test
    public void testUnserialize() throws Exception {
        System.out.println("unserialize");
        ItemSerializer instance = new ItemSerializer(new Item(0, 0));
        double expResult_h = 0.0;
        double expResult_m = 0.0;
        instance.unserialize();
        double result_h = instance.getItem().getH();
        assertEquals(expResult_h, result_h, 0.0);
        double result_m = instance.getItem().getM();
        assertEquals(expResult_m, result_m, 0.0);

    }

    /**
     * Test of getItem method, of class ItemSerializer.
     */
    @Test
    public void testGetItem() throws Exception {
        System.out.println("getItem");
        Item test_item = new Item(0, 0);
        ItemSerializer instance = new ItemSerializer(test_item);
        Item result = instance.getItem();
        assertEquals(test_item, result);
    }

}
