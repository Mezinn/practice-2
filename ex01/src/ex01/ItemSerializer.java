/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex01;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author mezinn
 */
public class ItemSerializer {

    private static final String serialized_file_name = "serialize_data.bin";

    private Item item;

    /*
    * Конструктор, що приймає об'єкт для серіалізації 
     */
    public ItemSerializer(Item item) {
        this.item = item;
    }

    /*
    * Функція для серіалізації об'єкту
     */
    public void serialize() throws FileNotFoundException, IOException {
        try (ObjectOutputStream object_output_stream = new ObjectOutputStream(new FileOutputStream(serialized_file_name))) {
            object_output_stream.writeObject(this.item);
            object_output_stream.flush();
            object_output_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    /*
    * Функція для дескріалізації об'єкту
     */
    public void unserialize() throws FileNotFoundException, IOException, ClassNotFoundException {
        try (ObjectInputStream object_input_stream = new ObjectInputStream(new FileInputStream(serialized_file_name))) {
            this.item = (Item) object_input_stream.readObject();
            object_input_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    public Item getItem() throws Exception {
        if (this.item != null) {
            return this.item;
        } else {
            throw new Exception("Item is undefined. It must be unserialized.");
        }
    }

}
